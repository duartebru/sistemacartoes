package br.com.itau.SistemaCartao.Controllers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.SistemaCartao.Service.FaturaService;
import br.com.itau.SistemaCartao.dto.Fatura;

@RestController
@RequestMapping("/fatura")
public class FaturaController {
	
	@Autowired
	FaturaService faturaService;
	
	@GetMapping("/{idCartao}/{dataInicial}/{dataFinal}")
	public Fatura consultarFatura(@PathVariable String idCartao, 
			@PathVariable @DateTimeFormat(iso=ISO.DATE) Date dataInicial, 
			@PathVariable @DateTimeFormat(iso=ISO.DATE) Date dataFinal) {
	
		return faturaService.consultarFatura(idCartao, dataInicial, dataFinal);		
	}	

}
