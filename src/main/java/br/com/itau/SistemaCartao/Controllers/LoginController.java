package br.com.itau.SistemaCartao.Controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.SistemaCartao.Service.AtendenteService;
import br.com.itau.SistemaCartao.Service.CartaoService;
import br.com.itau.SistemaCartao.dto.Atendente;
import br.com.itau.SistemaCartao.dto.Cartao;
import br.com.itau.SistemaCartao.dto.Usuario;
import br.com.itau.SistemaCartao.enu.Papel;
import br.com.itau.SistemaCartao.security.JwtTokenProvider;

@RestController
@RequestMapping("/login")
public class LoginController {
	
	 @Autowired
	  private CartaoService cartaoService;
	 
	 @Autowired
	 private AtendenteService atendenteService;

	  @PostMapping("/cliente")
	  public Map<String, Object> loginCliente(@RequestBody Cartao cartao) {
		
	    Cartao cartaoSalvo = cartaoService.fazerLogin(cartao);
	    
	    if(cartaoSalvo == null) {
	      throw new ResponseStatusException(HttpStatus.FORBIDDEN);
	    }
	    
	    Map<String, Object> retorno = new HashMap<>();
	    retorno.put("cliente", cartaoSalvo);
	    
	    JwtTokenProvider provider = new JwtTokenProvider();
	    Usuario user = new Usuario(cartao.getIdCartao(), Papel.CLIENTE);
	    
	    String token = provider.criarToken(user);
	    retorno.put("token", token);
	    
	    return retorno;
	  }
	  
	  @PostMapping("/atendente")
	  public Map<String, Object> loginAtendente(@RequestBody Atendente atendente) {
	    Atendente atendenteSalvo = atendenteService.fazerLogin(atendente);
	    
	    if(atendenteSalvo == null) {
	      throw new ResponseStatusException(HttpStatus.FORBIDDEN);
	    }
	    
	    Map<String, Object> retorno = new HashMap<>();
	    retorno.put("atendente", atendenteSalvo);
	    
	    JwtTokenProvider provider = new JwtTokenProvider();
	    Usuario user = new Usuario(atendente.getCpf(), Papel.CLIENTE);
	    
	    String token = provider.criarToken(user);
	    retorno.put("token", token);
	    
	    return retorno;
	  }
}
