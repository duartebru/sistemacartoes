package br.com.itau.SistemaCartao.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.SistemaCartao.Service.BandeiraService;
import br.com.itau.SistemaCartao.dto.Bandeira;

@RestController
@RequestMapping("/bandeira")
public class BandeiraController {
	
	@Autowired
	private BandeiraService bandeiraService;
	
	@PostMapping
	public Bandeira cadastrarBandeira(@RequestBody Bandeira bandeira) {
		return bandeiraService.setBandeira(bandeira);
	}
	
	@GetMapping
	public Bandeira getBandeira(@PathVariable int idBandeira) {
		Bandeira bandeira = bandeiraService.getBandeira(idBandeira);
		if(bandeira == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		return bandeira;
	}

}
