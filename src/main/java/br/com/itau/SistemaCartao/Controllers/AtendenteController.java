package br.com.itau.SistemaCartao.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.SistemaCartao.Service.AtendenteService;
import br.com.itau.SistemaCartao.dto.Atendente;

@RestController
@RequestMapping("/atendente")
public class AtendenteController {
	
	@Autowired
	AtendenteService atendenteService;
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Atendente setAtendente(@RequestBody Atendente atendente){
		return atendenteService.setAtendente(atendente);		
	}
}
