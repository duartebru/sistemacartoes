package br.com.itau.SistemaCartao.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.SistemaCartao.Service.CartaoService;
import br.com.itau.SistemaCartao.dto.Cartao;


@RestController
@RequestMapping("/cartao")
public class CartaoController {

	@Autowired
	private CartaoService cartaoService;
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Cartao cadastrarCartao(@RequestBody Cartao cartao) {
		return cartaoService.setCartao(cartao);		
	}
	
	@PatchMapping("/ativa/{numCartao}")
	public Cartao ativarCartao(@PathVariable String numCartao) {
		Cartao cartao = cartaoService.ativarCartao(numCartao);
		if(cartao == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		return cartao;	
	}
	
	@PatchMapping("/desativa/{numCartao}")
	public Cartao desativarCartao(@PathVariable String numCartao) {
		Cartao cartao = cartaoService.desativarCartao(numCartao);
		if(cartao == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		return cartao;		
	}	
}
