package br.com.itau.SistemaCartao.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.SistemaCartao.Service.LancamentoService;
import br.com.itau.SistemaCartao.dto.Lancamento;

@RestController
@RequestMapping("/lancamento")
public class LancamentoController {
		
	@Autowired
	LancamentoService lancamentoService;
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Lancamento setLancamento(@RequestBody Lancamento lancamento) {
		lancamento = lancamentoService.setLancamento(lancamento);
		if(lancamento == null) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		}	
		return lancamento;
	}
	
	@DeleteMapping("/{idLancamento}")
	public void deletarLancamento(@PathVariable int idLancamento) {		
		 lancamentoService.deletarLancamento(idLancamento);
	}		

}
