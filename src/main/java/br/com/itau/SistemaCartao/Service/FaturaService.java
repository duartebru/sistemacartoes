package br.com.itau.SistemaCartao.Service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.SistemaCartao.dto.Cartao;
import br.com.itau.SistemaCartao.dto.Fatura;
import br.com.itau.SistemaCartao.dto.Lancamento;

@Service
public class FaturaService {

	Fatura fatura;
	@Autowired
	LancamentoService lancamentoService;
	@Autowired
	CartaoService cartaoService;
	
	Iterable <Lancamento> lancamentos;
	
	public Fatura consultarFatura(String idCartao, Date dataInicial, Date dataFinal) {
		Cartao cartao = cartaoService.getCartao(idCartao);
		lancamentos = lancamentoService.consultarLancamentoCartao(cartao, dataInicial, dataFinal);
		return new Fatura(lancamentos);
	//	return fatura;
	}
	
}
