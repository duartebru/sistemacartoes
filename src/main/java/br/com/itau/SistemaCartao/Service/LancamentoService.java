package br.com.itau.SistemaCartao.Service;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.SistemaCartao.dto.Cartao;
import br.com.itau.SistemaCartao.dto.Lancamento;
import br.com.itau.SistemaCartao.repositories.LancamentoRepository;

@Service
public class LancamentoService {
	
	@Autowired
	LancamentoRepository lancamentoRepository;
	
	@Autowired
	CartaoService cartaoService;
		
	public Lancamento setLancamento(Lancamento lancamento) {		
		Cartao cartao = cartaoService.getCartao(lancamento.getCartao().getIdCartao());
		lancamento.setCartao(cartao);
		if (cartao.isAtivo()){
			Date data = new Date();
			lancamento.setData(data);		
			return lancamentoRepository.save(lancamento);			
		}		
		return null;
	}	
	
	public Lancamento getLancamento(int idLancamento) {
		Optional <Lancamento> lancamento = lancamentoRepository.findById(idLancamento);
		if (lancamento.isPresent()) {
			return lancamento.get();
		}
		return null;		
	}
	
	public void deletarLancamento(int IdLancamento) {
		lancamentoRepository.deleteById(IdLancamento);
	}
	
	public Iterable<Lancamento> consultarLancamentoCartao(Cartao cartao, Date dataInicial, Date dataFinal) {
		return lancamentoRepository.findAllByCartaoAndDataBetween(cartao, dataInicial, dataFinal);
	}	
}
