package br.com.itau.SistemaCartao.Service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.itau.SistemaCartao.dto.Cartao;
import br.com.itau.SistemaCartao.repositories.CartaoRepository;

@Service
public class CartaoService {

	@Autowired
	private CartaoRepository cartaoRepository;

	public Cartao setCartao(Cartao cartao) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		    
		cartao.setSenha(encoder.encode(cartao.getSenha()));
		return cartaoRepository.save(cartao);
	}
	
	public Cartao getCartao(String numCartao) {
		Optional<Cartao> cartao = cartaoRepository.findById(numCartao);
		if(cartao.isPresent()) {
			return cartao.get();
		}
		return null;		
	}
	
	public Cartao ativarCartao(String numCartao) {
		Cartao cartao = getCartao(numCartao);
		cartao.setAtivo(true);
		return cartaoRepository.save(cartao);
	}
	
	public Cartao desativarCartao(String numCartao) {
		Cartao cartao = getCartao(numCartao);
		cartao.setAtivo(false);
		return cartaoRepository.save(cartao);
	}
	
	public Cartao fazerLogin(Cartao cartao){
	    Optional<Cartao> cartaoOptional =  cartaoRepository.findById(cartao.getIdCartao());
	    
	    if(!cartaoOptional.isPresent()) {
	      return null;
	    }
	    
	    Cartao cartaoSalvo = cartaoOptional.get();
	    
	    BCryptPasswordEncoder bPasswordEncoder = new BCryptPasswordEncoder();
	    
	    if(bPasswordEncoder.matches(cartao.getSenha(), cartaoSalvo.getSenha())) {
	      return cartaoSalvo;
	    }
	    
	    return null;
	  }
	
}
