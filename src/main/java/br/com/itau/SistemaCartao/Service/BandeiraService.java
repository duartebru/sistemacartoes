package br.com.itau.SistemaCartao.Service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.SistemaCartao.dto.Bandeira;
import br.com.itau.SistemaCartao.repositories.BandeiraRepository;

@Service
public class BandeiraService {
	
	@Autowired
	private BandeiraRepository bandeiraRepository;
	
	public Bandeira setBandeira(Bandeira bandeira) {
		return bandeiraRepository.save(bandeira);
	}
	
	public Bandeira getBandeira(int idBandeira) {
		Optional<Bandeira> bandeira = bandeiraRepository.findById(idBandeira);
		if(bandeira.isPresent()) {
			return bandeira.get();
		}
		return null;			
	}
}
