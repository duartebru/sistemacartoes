package br.com.itau.SistemaCartao.Service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.SistemaCartao.dto.Cliente;
import br.com.itau.SistemaCartao.repositories.ClienteRepository;

@Service
public class ClienteService {
	
	@Autowired
	private ClienteRepository clienteRepository;

	public Cliente setCliente(Cliente cliente) {
		return clienteRepository.save(cliente);
	}
	
	public Cliente getCliente(String idCliente) {		
		Optional<Cliente> cliente = clienteRepository.findById(idCliente);
		if(cliente.isPresent()) {
				return cliente.get();
		}
		return null;			
	}
	
	public Cliente alterarCliente(String idCliente, Cliente clienteAlter) {
		Cliente cliente = getCliente(idCliente);
		
		if (clienteAlter.getNome() != null && !clienteAlter.getNome().isEmpty()) {
			cliente.setNome(clienteAlter.getNome());
		}
		if (clienteAlter.getEndereco() != null && !clienteAlter.getEndereco().isEmpty()) {
			cliente.setEndereco(clienteAlter.getEndereco());
		}			
		clienteRepository.save(cliente);
		return cliente;		
	}	
	
}
