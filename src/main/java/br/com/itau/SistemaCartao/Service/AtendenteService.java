package br.com.itau.SistemaCartao.Service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.itau.SistemaCartao.dto.Atendente;
import br.com.itau.SistemaCartao.repositories.AtendenteRepository;

@Service
public class AtendenteService {
	
	@Autowired
	AtendenteRepository atendenteRepository;

	 public Atendente setAtendente(Atendente atendente) {
		    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();		    
		    atendente.setSenha(encoder.encode(atendente.getSenha()));		    
		    return atendenteRepository.save(atendente);
	}
	 
	 public Atendente fazerLogin(Atendente atendente){
		    Optional<Atendente> atendenteOptional = 
		        atendenteRepository.findById(atendente.getCpf());
		    
		    if(!atendenteOptional.isPresent()) {
		      return null;
		    }
		    
		    Atendente atendenteSalvo = atendenteOptional.get();		    
		    BCryptPasswordEncoder bPasswordEncoder = new BCryptPasswordEncoder();		    
		    if(bPasswordEncoder.matches(atendente.getSenha(), atendenteSalvo.getSenha())) {
		      return atendenteSalvo;
		    }		    
		    return null;
		 }
	
}
