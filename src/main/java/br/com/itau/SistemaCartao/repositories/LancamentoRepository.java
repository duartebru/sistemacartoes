package br.com.itau.SistemaCartao.repositories;

import java.util.Date;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.SistemaCartao.dto.Cartao;
import br.com.itau.SistemaCartao.dto.Lancamento;

public interface LancamentoRepository extends CrudRepository<Lancamento, Integer>{
	public Iterable<Lancamento> findAllByCartaoAndDataBetween(Cartao cartao, Date dataInicial, Date dataFinal);
}
