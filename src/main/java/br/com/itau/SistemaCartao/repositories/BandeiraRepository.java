package br.com.itau.SistemaCartao.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.SistemaCartao.dto.Bandeira;

public interface BandeiraRepository extends CrudRepository<Bandeira, Integer>{

}
