package br.com.itau.SistemaCartao.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.SistemaCartao.dto.Cartao;

public interface CartaoRepository extends CrudRepository<Cartao, String>{

}
