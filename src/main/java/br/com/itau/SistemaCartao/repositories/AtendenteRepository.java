package br.com.itau.SistemaCartao.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.SistemaCartao.dto.Atendente;

public interface AtendenteRepository  extends CrudRepository<Atendente, String> {

}
