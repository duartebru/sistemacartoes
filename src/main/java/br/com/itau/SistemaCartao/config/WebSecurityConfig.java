package br.com.itau.SistemaCartao.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import br.com.itau.SistemaCartao.enu.Papel;
import br.com.itau.SistemaCartao.security.JwtFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{
	
 
  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf().disable()
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .authorizeRequests()
        .antMatchers("/cliente/*", "/atendente/*", "/bandeira/*", "/cartao/**/*").hasRole(Papel.ATENDENTE.name())
        .antMatchers("/fatura/*", "/lancamento/*").hasRole(Papel.CLIENTE.name())
        .and()
        .addFilterBefore(new JwtFilter(), UsernamePasswordAuthenticationFilter.class);
  }
}
