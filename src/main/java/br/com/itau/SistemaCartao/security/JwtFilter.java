package br.com.itau.SistemaCartao.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import br.com.itau.SistemaCartao.dto.Usuario;

public class JwtFilter extends OncePerRequestFilter {

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {
    JwtTokenProvider tokenProvider = new JwtTokenProvider();
    
    String token = parseToken(request);
    
    if(tokenProvider.validarToken(token)) {
      Usuario usuario = tokenProvider.lerToken(token);
     
  // SimpleGrantedAuthority authority = new SimpleGrantedAuthority("ADMIN");
      
     SimpleGrantedAuthority authority = new SimpleGrantedAuthority(usuario.getPapel().name());
      
      List<GrantedAuthority> authorities = new ArrayList<>();
      authorities.add(authority);
      
      UsernamePasswordAuthenticationToken auth = 
          new UsernamePasswordAuthenticationToken(usuario.getUserId(), "", authorities);
      
      SecurityContextHolder.getContext().setAuthentication(auth);
    }
    
    filterChain.doFilter(request, response);
  }
  
  private String parseToken(HttpServletRequest req) {
    String bearerToken = req.getHeader("Authorization");
    if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
        return bearerToken.substring(7, bearerToken.length());
    }
    return null;
}

}
