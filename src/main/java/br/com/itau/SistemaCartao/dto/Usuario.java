package br.com.itau.SistemaCartao.dto;

import br.com.itau.SistemaCartao.enu.Papel;

public class Usuario {

	private String userId;
	private Papel papel;
	
	public Usuario(String userId, Papel papel) {
		this.userId = userId;
		this.papel = papel;	
		
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Papel getPapel() {
		return papel;
	}
	public void setPapel(Papel papel) {
		this.papel = papel;
	}
	
}
