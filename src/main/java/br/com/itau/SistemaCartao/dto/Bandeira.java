package br.com.itau.SistemaCartao.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Bandeira {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int Idbandeira;
	
	@NotNull
	private String bandeira;

	
	public int getIdbandeira() {
		return Idbandeira;
	}

	public void setIdbandeira(int idbandeira) {
		Idbandeira = idbandeira;
	}

	public String getBandeira() {
		return bandeira;
	}

	public void setBandeira(String bandeira) {
		this.bandeira = bandeira;
	}

}
