package br.com.itau.SistemaCartao.dto;


public class Fatura {

	private Iterable<Lancamento> lancamentos;
	
	private double valorTotal = 0.0;
	
	public Fatura(Iterable<Lancamento> lancamentos) {
		this.lancamentos = lancamentos;
		for(Lancamento lanc : this.lancamentos) {
			valorTotal =+ valorTotal + lanc.getValor();			
		}		
	}
	
	public double getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(double valorTotal) {
		this.valorTotal = valorTotal;
	}	
	public Iterable<Lancamento> getLancamentos() {
		return lancamentos;
	}

	public void setLancamentos(Iterable<Lancamento> lancamentos) {
		this.lancamentos = lancamentos;
	}
		
}
