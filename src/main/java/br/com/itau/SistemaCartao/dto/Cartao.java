package br.com.itau.SistemaCartao.dto;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
public class Cartao {

	@Id 
	private String idCartao;
	
	@ManyToOne
	@NotNull
	private Cliente cliente;
	
	@OneToOne
	@NotNull
	private Bandeira bandeira;
	
	@NotNull
	private boolean ativo = false;
	
	@NotBlank
	private String nomeUsuario;
	
	@NotBlank
    @JsonProperty(access = Access.WRITE_ONLY)
	private String senha;
		
	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
		
	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public String getIdCartao() {
		return idCartao;
	}

	public void setIdCartao(String cartao) {
		this.idCartao = cartao;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Bandeira getBandeira() {
		return bandeira;
	}

	public void setBandeira(Bandeira bandeira) {
		this.bandeira = bandeira;
	}
	
}
