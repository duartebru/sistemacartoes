package br.com.itau.SistemaCartao.Service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.itau.SistemaCartao.dto.Cliente;
import br.com.itau.SistemaCartao.repositories.ClienteRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ClienteService.class)
public class ClienteServiceTest {
	
	@Autowired
	private ClienteService sujeito;
	
	@MockBean
	private ClienteRepository clienteRepository;
	
	private Cliente cliente; 
	private String cpf = "123123";
	
	@Before
	public void preparar() {
		cliente = new Cliente();
		cliente.setCpf("368.994.418-02");
		cliente.setNome("Tatian");
		cliente.setEndereco("Rua x");
	}
	

	@Test
	public void deveRetornarUmCliente() {
		when(clienteRepository.findById(cliente.getCpf())).thenReturn(Optional.of(cliente));
		
		//when		
		Cliente clienteSalva = sujeito.getCliente(cliente.getCpf());
		
		//expect
		assertEquals(cliente, clienteSalva);			
	}
	
	@Test
	public void deveRetornarNulo() {		
		when(clienteRepository.findById(cpf)).thenReturn(Optional.empty());
		
		//when		
		Cliente clienteSalva = sujeito.getCliente(cpf);
		
		//expect
		assertNull(clienteSalva);			
	}
	
	@Test
	public void deveSalvarUmCliente() {		
		when(clienteRepository.save(cliente)).thenReturn(cliente);
		
		//when		
		Cliente clienteSalva = sujeito.setCliente(cliente);
		
		//expect
		assertEquals(cliente, clienteSalva);
			
	}
	
	@Test
	public void deveAlterarUmCliente() {
		
	    when(clienteRepository.findById(cliente.getCpf())).thenReturn(Optional.of(cliente));
		
	    cliente.setEndereco("Rua y");
		//when		
		Cliente clienteSalva = sujeito.alterarCliente(cliente.getCpf(), cliente);
		
		//expect
		assertEquals(cliente, clienteSalva);			
		
	}
	
	@Test
	public void deveAlterarUmClienteComNomeEEnderecoNulo() {
		
	    when(clienteRepository.findById(cliente.getCpf())).thenReturn(Optional.of(cliente));
		
	    cliente.setEndereco(null);
	    cliente.setNome(null);
		//when		
		Cliente clienteSalva = sujeito.alterarCliente(cliente.getCpf(), cliente);
		
		//expect
		assertEquals(cliente, clienteSalva);			
		
	}
	
	
	@Test
	public void deveAlterarUmClienteComNomeNuloEnderecoVazio() {
		
	    when(clienteRepository.findById(cliente.getCpf())).thenReturn(Optional.of(cliente));
		
	    cliente.setEndereco("");
	    cliente.setNome(null);
		//when		
		Cliente clienteSalva = sujeito.alterarCliente(cliente.getCpf(), cliente);
		
		//expect
		assertEquals(cliente, clienteSalva);			
		
	}
	
	@Test
	public void deveAlterarUmClienteComNomeVazioEnderecoNulo() {
		
	    when(clienteRepository.findById(cliente.getCpf())).thenReturn(Optional.of(cliente));
		
	    cliente.setEndereco(null);
	    cliente.setNome("");
		//when		
		Cliente clienteSalva = sujeito.alterarCliente(cliente.getCpf(), cliente);
		
		//expect
		assertEquals(cliente, clienteSalva);			
		
	}
	
	@Test
	public void deveAlterarUmClienteComNomeVazioEnderecoVazio() {
		
	    when(clienteRepository.findById(cliente.getCpf())).thenReturn(Optional.of(cliente));
		
	    cliente.setEndereco("");
	    cliente.setNome("");
		//when		
		Cliente clienteSalva = sujeito.alterarCliente(cliente.getCpf(), cliente);
		
		//expect
		assertEquals(cliente, clienteSalva);			
		
	}
}
