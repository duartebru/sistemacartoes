package br.com.itau.SistemaCartao.Controller;

import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import org.springframework.security.test.context.support.WithMockUser;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.itau.SistemaCartao.Controllers.ClienteController;
import br.com.itau.SistemaCartao.Service.ClienteService;
import br.com.itau.SistemaCartao.dto.Cliente;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ClienteController.class)
public class ClienteControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private ClienteService clienteService;
	
	private String cpf = "123456789";
	Cliente cliente;
	
	private ObjectMapper mapper = new ObjectMapper();
	
	@Before
	public void preparar() {
		cliente = new Cliente();
		cliente.setCpf(cpf);
	}
	
	@Test
	@WithMockUser(roles="ATENDENTE")
	public void deveRetornarUmCliente() throws Exception{	
		
		when(clienteService.getCliente(cpf)).thenReturn(cliente);
		
		mockMvc.perform(get("/cliente/" + cpf))
		       .andExpect(status().isOk());
	}
	
	@Test
	@WithMockUser(roles="ATENDENTE")
	public void deveRetornarNulo() throws Exception{
	
		when(clienteService.getCliente(cpf)).thenReturn(null);
		
		mockMvc.perform(get("/cliente/" + cpf))
		       .andExpect(status().isNotFound());		  
	}
	
	
	@Test
	@WithMockUser(roles="ATENDENTE")
	public void deveSalvarUmCliente() throws Exception{
	
		when(clienteService.setCliente(any(Cliente.class))).thenReturn(cliente);
		
		String clienteJson = mapper.writeValueAsString(cliente);
		
		mockMvc.perform(
				post("/cliente")
				.content(clienteJson)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				)
		       .andExpect(status().isCreated())
		       .andExpect(content().string(clienteJson));
	}
	
	@Test
	@WithMockUser(roles="ATENDENTE")
	public void deveAlterarUmCliente() throws Exception{
		cliente.setEndereco("Rua x");	
	
		when(clienteService.alterarCliente(any(String.class), any(Cliente.class))).thenReturn(cliente);
		
		String clienteJson = mapper.writeValueAsString(cliente);
		
		mockMvc.perform(
				patch("/cliente/" + cpf)
				.content(clienteJson)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				)
		       .andExpect(status().isOk())
		       .andExpect(content().string(clienteJson));
	}
	
	@Test
	@WithMockUser(roles="ATENDENTE")
	public void deveRetornarNuloAoTentarAlterarClienteQueNaoExiste() throws Exception{
		cliente.setEndereco("Rua x");	
	
		when(clienteService.alterarCliente(cpf, cliente)).thenReturn(cliente);
		
		String clienteJson = mapper.writeValueAsString(cliente);
		
		mockMvc.perform(
				patch("/cliente/" + cpf)
				.content(clienteJson)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				)
		       .andExpect(status().isNotFound());
		       
	}	

}
